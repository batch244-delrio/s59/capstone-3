
import { Card, Button, Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    const { _id, name, description, price } = productProp;

    return ( 
		<Container className='mt-4'>  
			<Row className='text-center'>   
				<Col>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Button as={Link} variant="primary" to={`/products/${_id}`}>Details</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row> 
		</Container> 
    )
}

