import { useState, useEffect, useContext } from 'react';
import { Form, Button, FloatingLabel } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

  const {user} = useContext(UserContext);

  const navigate = useNavigate();
  
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState(''); 

  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password1);
  console.log(password2);


  function registerUser(e) {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password1
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data === true){
        Swal.fire({
          title: "Registration successful",
          icon: "success",
          text: "Lorem ipsum dolor sit amet!"
        })

        setEmail('');
        setPassword1('');
        setPassword2('');

      } else {
        Swal.fire({
          title: "User already exists",
          icon: "error",
          text: "Please provide another email to complete the registration."
        })
      }
      navigate("/login");       
    })
  }

  useEffect(() => {
    if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);

  return (
    (user.id !== null)
    ?
      <Navigate to="/products"/>
      
    :

    <Form onSubmit={(e) => registerUser(e)}>

      <Form.Group controlId = "userEmail">
        <FloatingLabel
                       controlId="floatingEmail"
                           label="Email Address"
                           className="mb-3">
        <Form.Control 
          type="email"
          placeholder="Email Address"
          required
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
        </FloatingLabel>
      </Form.Group>

      <Form.Group controlId = "password1">
        <FloatingLabel
                       controlId="floatingPassword1"
                           label="Password"
                           className="mb-3">
        <Form.Control 
          type="password"
          placeholder="Password"
          required
          value={password1}
          onChange={e => setPassword1(e.target.value)}
        />
        </FloatingLabel>
      </Form.Group>

      <Form.Group className="mb-3" controlId = "password2">
        <FloatingLabel
                       controlId="floatingPassword2"
                           label="Verify Password"
                           className="mb-3">
        <Form.Control          
          type="password"
          placeholder="Verify Password"
          required
          value={password2}
          onChange={e => setPassword2(e.target.value)}
        />
        </FloatingLabel>
      </Form.Group>
      
      {isActive
        ?
          <Button variant="success" type="submit" id="submitBtn" style={{ width: "100%" }}>Submit</Button>
        :   
          <Button variant="secondary" type="submit" id="submitBtn" style={{ width: "100%" }} disabled>Submit</Button>
      }
    </Form>
  )
}
