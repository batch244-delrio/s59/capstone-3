import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { productId } = useParams();

	// To be able to obtain the user ID so that a user can order
	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);
	const [ totalAmount, setTotalAmount ] = useState(0);

	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the product from the database to be displayed in the "ProductView" page
		fetch(`${process.env.REACT_APP_API_URL}/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	useEffect(() => {
		setTotalAmount(price * quantity);
	}, [quantity, price]);

	const order = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Order successful!",
					icon: "success",
					text: "You have successfully ordered the product!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const handleDecrement = () => {
		if (quantity > 0) {
			setQuantity(quantity - 1);
		}
	}

	const handleIncrement = () => {
		setQuantity(quantity + 1);
	}

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body >
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Quantity:</Card.Subtitle>
					        <div className="input-group mt-2 mb-3">
					        	<Button type="button" onClick={handleDecrement} className="input-group-text">-</Button>
					        	<div className="form-control text-center">{quantity}</div>
					        	<Button type="button" onClick={handleIncrement} className="input-group-text">+</Button>
					        </div>
					        <div className="d-flex justify-content-between align-items-center">
					        	<Card.Subtitle>Total amount:</Card.Subtitle>
					        	<Card.Text>Php {totalAmount}</Card.Text>
					        </div>
					        {user.id !== null
					        	?
					        	

					        	<Button variant="primary" onClick={() => order(productId)}>Checkout</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to Order</Button>
					    	}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
