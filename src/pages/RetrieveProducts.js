import { useState } from 'react';
import { Button, Table } from 'react-bootstrap';

export default function ProductList() {
  const [products, setProducts] = useState([]);

  function retrieveProducts() {
    // Fetch products from the database and update state
    fetch(`${process.env.REACT_APP_API_URL}/activeProducts`)
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(error => console.log(error));
  }

  return (
    <div>
      <Button onClick={retrieveProducts}>Retrieve Products</Button>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product => (
            <tr key={product.id}>
              <td>{product._id}</td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}


